module gitlab.wikimedia.org/frankie/aqsassist

go 1.17

require (
	github.com/asaskevich/govalidator v0.0.0-20230301143203-a9d515a09cc2
	github.com/grafadruid/go-druid v0.0.6
	github.com/stretchr/testify v1.8.1
	golang.org/x/text v0.13.0
	gopkg.in/yaml.v2 v2.4.0
	schneider.vip/problem v1.8.1
)

require (
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.4 // indirect
	github.com/magefile/mage v1.15.0 // indirect
	github.com/tj/assert v0.0.3 // indirect
)

require (
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/hhsnopek/etag v0.0.0-20171206181245-aea95f647346
	github.com/klauspost/compress v1.17.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1
)
