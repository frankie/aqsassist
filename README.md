# aqs-assist

AQS Assist is an internal library developed and maintained by the Wikimedia Foundation that contains functions designed to operate across AQS 2.0 microservices.

It has two modules:
- **aqsassist** (aqsassist.go): It contains common functions to manage and validate enumerated values and dates 
- **druid_utils** (druid_utils.go): It contains common functions to build and run Druid queries

Some details about both modules can be found below

## aqsassist

### ValidateTimestamp(date string) (string, error)

It checks that _date_ matches the YYYYMMDDHH pattern. Before that, if needed, "00" is added as an hour suffix. If validation is ok the date (with no error) is returned (with the hour suffix if added)
Otherwise, an empty value and an error will be returned

### TrimProjectDomain(project param) string

Normalize project name (lower case and removing ".org" suffix and "www." prefix)

### ValidateProject(project string) (bool, string)

Validate and normalize a project

### ValidateReferer(referer string) (bool, string)

Validate and normalize a referer

### NormalizePageTitle(pageTitle string) string

Normalize a page title, replacing spaces with underscores. If you provide, for instance "Museo del Prado", this function returns "Museo_del_Prado"

### SetSecurityHeaders

### FilterAgent

### ValidateDuration

### StartBeforeEnd

### CreateProblem

### GetTime(year, month, day string) time.Time

It returns a date, as a time.Time object, for the given year, month and day as string values.

If date is equals to "all-days", day value will consider equals to 1 because it's useful when aggregating results with 
"monthly" granularity. In these cases the first day of the month is the date that is shown in the response (e.g.: "2023-02-01T00:00:00Z")

### GetMarshallTextTime(year, month, day string) string

It returns a date as a string, and following the RFC 3339 format, given a year, month and day as string values.
For example:
  - If "2023", "03", "13" is provided: "2023-02-23T00:00:00Z" will be returned

### GetStartEndDate(year, month, day string) (time.Time, time.Time)

It returns two dates, _start_ and _end_ as time.Time objects, for a given day of month (or the whole month if 'all-days' string is provided as the day value).
For example:
  - If "2023", "03", "23" is provided: _start_ and _end_ will be "2023-03-23" and "2023-02-24" respectively, both as time.Time objects
  - If "2023", "10", "all-days" is provided: _end_ and _end_ will be "2023-10-01" and "2023-11-01" respectively, both as time.Time objects

### IsYear(year string) bool

It validates whether a year is valid. It checks:
  - Four digits integer value 

### IsMonth(month string) bool

It validates whether a month is valid. It checks:
  - Two digits integer value (e.g.: "01" will be considered as a valid value and "1" as an invalid one)
  - As an integer value, its range is between 1 and 12

### IsDay(year, month, day string, allDaysSupport bool) bool

It validates whether a day is valid. It checks:
  - Two digits integer value ("01", "02", . . . "31")
  - As an integer value, its range is between 1 and the last day for the provided month and year
  - If _allDaysSupport_ is true, "all-days" string is consider as a valid value

### GetDaysIn(year, month string) int

It returns which is the last day for the month and year provided. It also can be used to know how many days there are for a specific month

### ValidateFullMonthsBetween(start, end string, untilFirstDayNextMonth) (string, string, error)

It validates dates (and adjust them when available) to ensure dates range include only full months.
Both dates must be in YYYYMMDDHH format.

To consider:
  - start date could be changed to the first day of the next month if it doesn't specify a full month (e.g: if you specify "20230303" as start date, it will be changed to "20230401")
  - end date will be adjusted to the first day/last day of the same month (depending on the flag _untilFirstDayOfMonth_). For example, if "2023051200" is provided, it will be adjusted to "2023050100" before return it
  - The flag _untilFirstDayNextMonth_ will be true if we consider that next month's first day is the minimal amount of time to consider a full month (e.g.: 20230201 as _start_ and 20230301 as _end_). It will be false if we consider that the last day of the start month is enough to consider an entire one (e.g: 20230101 as _start_ and 20230131 as _end_) 

### IsEditorType(editorType string) bool

It checks whether editorType has a valid value ("all-editor-types", "anonymous", "group-bot", "name-bot" or "user")

### IsPageType(pageType string) bool

It checks whether pageType has a valid value ("all-page-types", "content" or "non-content")

### IsActivityLevel(activityLevel string) bool

It checks whether activityLevel has a valid value ("1..4-edits", "5..24-edits", "25..99-edits", or "100..-edits")

### IsGranularity(granularity string) bool

It checks whether granularity has a valid value ("daily" or "monthly")

### IsMediaType(mediaType string) bool

It checks whether mediaType has a valid value ("all-media-types", "image", "video", "audio", "document" or "other")

### IsAgent(agent string) bool

It checks whether agent has a valid value ("all-agents", "user" or "spider")

### IsAccessSite(accessSite string) bool

It checks whether agent has a valid value ("all-sites", "desktop-site" or "mobile-site")

## druid_utils

### GetEventType(granularity string) string

Get the right event type according to the specified granularity:
- If granularity is 'daily' the 'daily_digest' value is returned (as _DruidUtils.EventType.DailyDigest_)
- If granularity is 'monthly' the 'monthly_digest' value is returned (as _DruidUtils.EventType.MonthlyDigest_)

### GetGranularity(days string) string

Given the expression 'all-days' or a day number, right granularity is returned as a string
- If days is 'all-days' the 'monthly' value is returned
- Any other case (days is a number) the 'daily' value is returned

### ProcessTimeseriesQuery(parameters DruidQueryParams, datasourceName string, druidClient *druid.Client) ([]ResultsTuple, error)

Run a _Timeseries_ query based on the query params, the datasource name and the current Druid connection.
As a result it returns a _ResultsTuple_ structure with all the data we need to compose the final response in the data layer.

- **parameters** is a _DruidQueryParams_ structure that contains all the required request and Druid parameters to build the query. 
- **datasourceName** is the name of the Druid datasource
- **druidClient** is the current Druid connection

Assuming the following code belongs to the data layer for a sample _aggregate_ endpoint, the first step before calling the function would be to populate the _parameters_ structure with the right request and Druid parameters. 
Keep in mind that _project_, _editorType_, _pageType_, _activityLevel_, _granularityValue_, _start_ and _end_ are the values as is they come in the request

```go
var parameters = aqsassist.DruidQueryParams{
    Project:          project,
    EditorType:       editorType,
    PageType:         pageType,
    ActivityLevel:    activityLevel,
    Granularity:      granularityValue,
    Start:            start,
    End:              end,
    EventEntity:      aqsassist.Druid.EventEntity.User,
    EventType:        aqsassist.GetEventType(granularityValue),
    AggregationType:  aqsassist.Druid.Aggregated.Sum,
    AggregationField: aqsassist.Druid.Metric.Events,
}
```

Next and final step is to call the function and collect the data to map it to the needed response structure (in this case _AggregateEditorResult_):

```go
data, err := aqsassist.ProcessTimeseriesQuery(parameters, config.Druid.Datasource, druidClient)
var results []entities.AggregateEditorResult
for _, result := range data {
    results = append(results, entities.AggregateEditorResult{
        Timestamp: result.Item,
        Editors:   result.Aggregation,
    })
}
```

### ProcessTopNQuery(parameters DruidQueryParams, datasourceName string, druidClient *druid.Client) ([]ResultsTuple, error)

Run a _TopN_ query based on the query params, the datasource name and the current Druid connection. As a result it returns
a _ResultsTuple_ with all the data we need to compose the final response in the data layer.

- **parameters** is a _DruidQueryParams_ structure that contains all the required request and Druid parameters to build the Druid query. For a sample _top_ endpoint it would be as follows:
- **datasourceName** is the name of the Druid datasource
- **druidClient** is the current Druid connection

Assuming the following code belongs to the data layer for a sample _top_ endpoint, the first step before calling the function would be to populate the _parameters_ structure with the right request and Druid parameters.
Keep in mind that _project_, _editorType_, _pageType_, _year_, _month_ and _day_ are the values as is they come in the request

```go
var parameters = aqsassist.DruidQueryParams{
    Project:          project,
    EditorType:       editorType,
    PageType:         pageType,
    Year:             year,
    Month:            month,
    Day:              day,
    EventType:        aqsassist.Druid.EventType.Create,
    EventEntity:      aqsassist.Druid.EventEntity.Revision,
    AggregationType:  aqsassist.Druid.Aggregated.Sum,
    AggregationField: aqsassist.Druid.Metric.Events,
    TopDimension:     aqsassist.Druid.Dimension.UserText,
}
```

Next and final step is to call the function and collect the data to map it to the needed response structure (in this case _TopByEditsResultDetails_). In this sample case it's necessary to add also the rank value:

```go
data, err := aqsassist.ProcessTopNQuery(parameters, config.Druid.Datasource, druidClient)
var results []entities.TopByEditsResultDetails
var rank = 1
for _, result := range data {
    results = append(results, entities.TopByEditsResultDetails{
        UserText: result.Item,
        Edits:    result.Aggregation,
        Rank:     rank,
    })
    rank++
}
```