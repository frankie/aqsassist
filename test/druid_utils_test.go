package test

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.wikimedia.org/frankie/aqsassist"
)

func TestMain(m *testing.M) {
	filename := "./druid-schemas.yaml"
	err := aqsassist.ReadDruidConfig(filename)
	if err != nil {
		_, _ = fmt.Fprintln(os.Stderr, "TestMain: ", err)
		os.Exit(1)
	}

	m.Run()
}

func TestReadConfig(t *testing.T) {
	assert.Equal(t, "event_type", aqsassist.Druid.Dimension.EventType)
	assert.Equal(t, "[self_created]", aqsassist.Druid.OtherTags.SelfCreated)
}

func TestGetEventType(t *testing.T) {
	assert.Equal(t, aqsassist.Druid.EventType.DailyDigest, aqsassist.GetEventType("daily"))
	assert.Equal(t, aqsassist.Druid.EventType.MonthlyDigest, aqsassist.GetEventType("monthly"))
}
